from django.contrib.auth.models import User
from django.test import RequestFactory, TestCase
from django.utils import timezone

from polls.models import Choice, Question


class PollModelTests(TestCase):
    """ Handles Polls model testing """

    def setUp(self):
        """ Create a sample user and question """
        self.factory = RequestFactory()
        self.author = User.objects.create_user(
            username="test",
            email="test@example.com",
            password="devpassword"
        )
        self.question = Question.objects.create(
            question_text="Test question",
            created_by=self.author,
        )

    def test_question_str_representation(self):
        """
        Test Question string representations
        :return True
        """
        self.assertEqual(str(self.question), "Test question")

    def test_choice_str_representation(self):
        """
        test Choices string represnetation
        :return True
        """
        choice = Choice.objects.create(
            question=self.question,
            choice_text="Test"
        )

        self.assertEqual(str(choice), 'Test')

    # def test_create_question(self):
    #     """ Test create a new question """
    #     question = sample_question()

    #     self.assertEquals(str(question), question.question_text)

    # def test_create_choices(self):
    #     """ Test create a new choice """
    #     choice = models.Choice.objects.create(
    #         question=sample_question(),
    #         choice_text="React",
    #     )

    #     self.assertEqual(str(choice), choice.choice_text)
