from django.contrib.auth import get_user_model
from django.http import response
from django.urls import reverse
from rest_framework.authtoken.models import Token
from rest_framework.test import APIClient, APIRequestFactory, APITestCase

from polls.models import Choice, Question
from polls.views import QuestionView

QUESTION_URI = reverse('questions:questions-list')


def specific_question(pk):
    """
    Handles geting url for specific questions
    :param pk: key
    """
    return reverse('questions:questions-detail', args=[pk])


def choice_list(pk):
    """
    Returns choice list url
    @param pk
    """
    return reverse('questions:choice_list', args=[pk])


def choice_vote(pk, choice_pk):
    """
    Returns choice list url
    @param pk
    @param choice_pk
    """
    return reverse('questions:create_vote', args=[pk, choice_pk])


class QuestionViewTest(APITestCase):
    """ Handles Polls API resqest testing
    : any authenticated user should be able to create a question
    : any visitor should be not be able to view questions
    """

    def setUp(self):
        """ Create a sample user and question """
        self.client = APIClient()
        # user 1
        self.user = self.setup_user()
        self.token = Token.objects.create(
            user=self.user
        )
        self.token.save()
        self.headers = {
            'HTTP_AUTHORIZATION': f'Token {self.token}'
        }
        # user 2
        self.user_1 = self.setup_user('dev1', 'dev1@e.c', 'dev123')
        self.token_1 = Token.objects.create(
            user=self.user_1
        )
        self.token_1.save()
        self.headers_1 = {
            'HTTP_AUTHORIZATION': f'Token {self.token_1}'
        }

    @staticmethod
    def setup_user(username="test",
                   email="test@example.com",
                   password="devpassword",
                   ):
        """
        Create a test user
        @param username: string
        @param email: string
        @param password: string

        @return User[object]
        """
        return get_user_model().objects.create_user(
            username=username,
            email=email,
            password=password
        )

    def create_question(self,
                        payload={"question_text": "What test?"},
                        headers={},
                        ):
        """ Return create a question success """
        return self.client.post(QUESTION_URI,
                                payload,
                                **headers,
                                format='json')

    def test_fetch_all_questions(self):
        """ Test fetching all questions """
        response = self.client.get(QUESTION_URI, **self.headers)
        self.assertEqual(response.status_code, 200,
                         'Expected Response Code 200, recieved {0} instead'
                         .format(response.status_code))

    def test_fetch_all_questions_no_auth(self):
        """"Test fetching questions without being authorized """
        response = self.client.get(QUESTION_URI)
        self.assertEqual(response.status_code, 401,
                         'Expected Response Code 401, recieved {0} instead'
                         .format(response.status_code))

    def test_create_question_success(self):
        """ Test create a question successfully """
        response = self.create_question(headers=self.headers)
        self.assertEqual(response.status_code, 201,
                         'Expected Response Code 201, recieved {0} instead'
                         .format(response.status_code))

    def test_create_question_fails_no_auth(self):
        """ Test create a question successfully """
        response = self.create_question()
        self.assertEqual(response.status_code, 401,
                         'Expected Response Code 401, recieved {0} instead'
                         .format(response.status_code))

    def test_create_question_payload_not_valid(self):
        """ test create question wwith invalid payload """
        response = self.create_question(payload=None, headers=self.headers)
        self.assertEqual(response.status_code, 400,
                         'Expected Response Code 400, recieved {0} instead'
                         .format(response.status_code))
        self.assertDictEqual(response.data, {
            "question_text": [
                "This field is required."
                ]}
            )

    def test_delete_own_question_success(self):
        """ Test delete own question successfully """
        question = self.create_question(headers=self.headers)
        url = specific_question(pk=question.data['id'])
        response = self.client.delete(url, **self.headers)

        self.assertEqual(response.status_code, 204,
                         'Expected Response Code 204, recieved {0} instead'
                         .format(response.status_code))

    def test_can_not_delete_others_question_successfully(self):
        """ Test can not delete others question successfully """
        question = self.create_question(headers=self.headers)
        url = specific_question(pk=question.data['id'])
        response = self.client.delete(url, **self.headers_1)

        self.assertEqual(response.status_code, 403,
                         'Expected Response Code 403, recieved {0} instead'
                         .format(response.status_code))
        self.assertDictEqual(response.data, {
            "detail": "You can not delete this Question"
            }
        )

    # Choices

    def create_choice(self,
                      payload={"choice_text": "test"},
                      headers={},
                      ):
        """ Returns create a choice success """
        question = self.create_question(headers=self.headers)
        url = choice_list(pk=question.data['id'])
        return self.client.post(url,
                                payload,
                                **headers,
                                format='json')

    def test_fetch_all_choice(self):
        """ Test fetching all choice """
        question = self.create_question(headers=self.headers)
        url = choice_list(pk=question.data['id'])

        response = self.client.get(url, **self.headers)
        self.assertEqual(response.status_code, 200,
                         'Expected Response Code 200, recieved {0} instead'
                         .format(response.status_code))

    def test_fetch_all_choice_no_auth(self):
        """"Test fetching choice without being authorized """
        question = self.create_question(headers=self.headers)
        url = choice_list(pk=question.data['id'])

        response = self.client.get(url)
        self.assertEqual(response.status_code, 401,
                         'Expected Response Code 401, recieved {0} instead'
                         .format(response.status_code))

    def test_create_choice_success(self):
        """ Test create a choice successfully """
        response = self.create_choice(headers=self.headers)
        self.assertEqual(response.status_code, 201,
                         'Expected Response Code 201, recieved {0} instead'
                         .format(response.status_code))

    def test_create_choice_fails_no_auth(self):
        """ Test create a choice successfully """
        response = self.create_choice()
        self.assertEqual(response.status_code, 401,
                         'Expected Response Code 401, recieved {0} instead'
                         .format(response.status_code))

    def test_create_choice_payload_not_valid(self):
        """ test create choice wwith invalid payload """
        response = self.create_choice(payload=None, headers=self.headers)
        self.assertEqual(response.status_code, 400,
                         'Expected Response Code 400, recieved {0} instead'
                         .format(response.status_code))
        self.assertDictEqual(response.data, {
            "choice_text": [
                "This field is required."
                ]}
            )

    def test_can_not_delete_others_question_successfully(self):
        """ Test can not delete others question successfully """
        response = self.create_choice(headers=self.headers_1)
        self.assertEqual(response.status_code, 403,
                         'Expected Response Code 403, recieved {0} instead'
                         .format(response.status_code))
        self.assertDictEqual(response.data, {
            "detail": "You can create choices for this Question"
            }
        )
