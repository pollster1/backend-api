from django.urls import include, path
from rest_framework.routers import DefaultRouter

from . import views

app_name = 'questions'

router = DefaultRouter()
router.register('questions', views.QuestionView, basename="questions")

urlpatterns = [
    path('questions/<int:pk>/choices/',
         views.ChoiceListView.as_view(), name="choice_list"),

    path('questions/<int:pk>/choices/<int:choice_pk>/vote/',
         views.CreateVoteView.as_view(), name="create_vote"),
]

urlpatterns += router.urls
