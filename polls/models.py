from datetime import datetime, timedelta

from django.contrib.auth.models import User
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _


class TimeStampModel(models.Model):
    created_at = models.DateTimeField(_('created at'), auto_now_add=True)
    updated_at = models.DateTimeField(
        _('updated at'), auto_now_add=False, auto_now=True)

    class Meta:
        abstract = True


class Question(TimeStampModel):
    question_text = models.CharField(max_length=200, null=False)
    pub_date = models.DateTimeField('date published', auto_now=True)
    created_by = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return self.question_text


class Choice(TimeStampModel):
    question = models.ForeignKey(Question,
                                 related_name="choices",
                                 on_delete=models.CASCADE,
                                 )
    choice_text = models.CharField(max_length=200)

    def __str__(self):
        return self.choice_text


class Vote(TimeStampModel):
    choice = models.ForeignKey(Choice,
                               related_name="votes",
                               on_delete=models.CASCADE)
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    voted_by = models.ForeignKey(User, on_delete=models.CASCADE)

    class Meta:
        unique_together = ("question", "voted_by")
