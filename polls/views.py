from django.http import request
from django.shortcuts import get_object_or_404
from rest_framework import generics, pagination, serializers, status, viewsets
from rest_framework.exceptions import PermissionDenied
from rest_framework.response import Response

from polls.models import Choice, Question
from polls.serializers import (ChoiceSerializer, QuestionSerializer,
                               VoteSerializer)


class CustomPagination(pagination.PageNumberPagination):
    page_size = 1
    ordering = '-id'


class QuestionView(viewsets.ModelViewSet):
    """
     Get the list of questions or create them.
     Retrieve an individual question, or delete the question.
     * Allows GET and POST, DELETE
    """
    queryset = Question.objects.all()
    serializer_class = QuestionSerializer
    pagination_class = CustomPagination

    def perform_create(self, serializer):
        if serializer.is_valid():
            question = serializer.save(created_by=self.request.user)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, *args, **kwargs):
        question = Question.objects.get(pk=self.kwargs['pk'])
        if not request.user == question.created_by:
            raise PermissionDenied('You can not delete this Question')

        return super().destroy(request, *args, **kwargs)


class ChoiceListView(generics.ListCreateAPIView):
    """
    Get the list of choices or create them.
    * Allows GET and POST
    """
    def get_queryset(self):
        """
        Get a list of choices linked to a specific question
        """
        queryset = Choice.objects.filter(question_id=self.kwargs['pk'])
        return queryset

    serializer_class = ChoiceSerializer

    def post(self, request, *args, **kwargs):
        question = Question.objects.get(pk=self.kwargs['pk'])
        if not request.user == question.created_by:
            raise PermissionDenied('You can create choices for this Question')

        serializer = ChoiceSerializer(data=request.data)
        if serializer.is_valid():
            question = serializer.save(question=question)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class CreateVoteView(generics.CreateAPIView):
    """
    Allows createing a vote, but not listing them.
    * Allows POST only
    """
    serializer_class = VoteSerializer

    def post(self, request, *args, **kwargs):
        question = get_object_or_404(Question, pk=self.kwargs['pk'])
        choice = get_object_or_404(Choice, pk=self.kwargs['choice_pk'])

        serializer = VoteSerializer(data={
            "choice": choice.pk,
            })
        if serializer.is_valid():
            vote = serializer.save(
                question=question,
                voted_by=request.user
            )
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
