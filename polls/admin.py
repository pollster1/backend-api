from django.contrib import admin

from .models import Choice, Question, Vote

admin.site.site_header = "Pollster Admin"
admin.site.site_title = "Pollster Admin Area"
admin.site.index_title = "Welcome to the Pollster Admin Area"


class ChoiceInLine(admin.TabularInline):
    model = Choice
    extra = 3


class ChoiceAdmin(admin.ModelAdmin):
    list_display = ('question', 'choice_text', 'created_at', 'updated_at',)
    autocomplete_fields = ['question']
    list_filter = ('created_at',)
    ordering = ['created_at', 'updated_at', 'choice_text']
    search_fields = ['choice_text']


class VoteAdmin(admin.ModelAdmin):
    list_display = ('question', 'choice', 'voted_by', 'created_at',)
    list_filter = ('voted_by', 'created_at', )
    ordering = ['voted_by', 'choice']
    search_fields = ['voted_by', 'question', 'choice', ]


class QuestionAdmin(admin.ModelAdmin):
    list_display = ('question_text', 'created_by', 'pub_date',)
    list_filter = ('created_by', 'pub_date')
    ordering = ['pub_date', 'created_by']
    search_fields = ['question_text']
    fieldsets = [
        (None, {'fields': ['question_text']}),
        ('Publisher', {'fields': ['created_by']}),
        ('Date Information', {'fields': ['pub_date']}),
        ]
    readonly_fields = ('pub_date',)
    inlines = [ChoiceInLine]


admin.site.register(Question, QuestionAdmin)
admin.site.register(Choice, ChoiceAdmin)
admin.site.register(Vote, VoteAdmin)
