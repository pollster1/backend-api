from authentication.serializers import UserSerializer
from rest_framework import serializers
from rest_framework.validators import UniqueValidator

from polls.models import Choice, Question, Vote


class VoteSerializer(serializers.ModelSerializer):
    """ Serializer for votes object """

    class Meta:
        model = Vote
        fields = '__all__'
        read_only_fields = ['question', 'voted_by']


class ChoiceSerializer(serializers.ModelSerializer):
    """ Serializer for choice object """
    votes = VoteSerializer(many=True, required=False)

    class Meta:
        model = Choice
        fields = '__all__'
        read_only_fields = ['question', 'votes']


class QuestionSerializer(serializers.ModelSerializer):
    """ Serializer for Question object """
    choices = ChoiceSerializer(many=True, read_only=True, required=False)

    question_text = serializers.CharField(
        required=True,
        validators=[UniqueValidator(
            queryset=Question.objects.all(),
            message='Can not have duplicate questions.'),
            ],
    )
    created_by = UserSerializer(read_only=True, required=False)

    class Meta:
        model = Question
        fields = '__all__'
        read_only_fields = ['created_by']
