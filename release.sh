#!/usr/bin/env bash
echo "Running Release Tasks"

echo "Running Database migrations and migrating the new changes"
python manage.py makemigrations authentication polls
python manage.py migrate --run-syncdb --noinput

echo "Done.."
