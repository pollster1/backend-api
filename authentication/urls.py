from django.urls import include, path
from rest_framework.routers import DefaultRouter

from . import views

app_name = 'authentication'

urlpatterns = [
    path('users/register/', views.UserRegisterView.as_view(), name="user_create"),
    path('users/login/', views.UserLoginView.as_view(), name="user_login")

]
