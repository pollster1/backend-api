import re

from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.core.validators import ValidationError
from rest_framework import serializers
from rest_framework.authtoken.models import Token
from rest_framework.validators import UniqueValidator


def email_validate(email):
    regex = "^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$"

    if not re.match(regex, email):
        raise ValidationError('The provided email is invalid')


class UserSerializer(serializers.ModelSerializer):
    """ Serializer for Register User object """
    email = serializers.EmailField(
        required=True,
        validators=[UniqueValidator(
            queryset=User.objects.all(),
            message='Provided email is already in use'),
            email_validate],
    )

    username = serializers.CharField(
        required=True,
        validators=[UniqueValidator(
            queryset=User.objects.all(),
            message='Provided Username is already in use'),
            ],
    )

    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'password')
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        """ Overriding user model create method to save user"""
        user = User(
            email=validated_data['email'],
            username=validated_data['username']
        )
        user.set_password(validated_data['password'])
        user.save()
        Token.objects.create(user=user)
        return user


class LoginSerializer(serializers.Serializer):
    """ Serializer for Login user object """
    username = serializers.CharField(max_length=255)
    password = serializers.CharField(max_length=128, write_only=True)

    def validate(self, data):
        # The `validate` method is where we make sure that the current
        # instance of `LoginSerializer` has "valid". In the case of logging a
        # user in, this means validating that they've provided an username
        # and password and that this combination matches one of the users in
        # our database.
        username = data.get('username', None)
        password = data.get('password', None)

        # As mentioned above, an username is required. Raise an exception if an
        # username is not provided.
        if username is None:
            raise serializers.ValidationError({
                  'username': 'An username address is required to log in.'
                  })

        # As mentioned above, a password is required. Raise an exception if a
        # password is not provided.
        if password is None:
            raise serializers.ValidationError({
                  'password': 'A password is required to log in.'
                  })

        # The `authenticate` method is provided by Django and handles checking
        # for a user that matches this username/password combination.
        user = authenticate(username=username, password=password)

        # If no user was found matching this username/password combination then
        # `authenticate` will return `None`. Raise an exception in this case.
        if user is None:
            raise serializers.ValidationError(
                'A user with this username and password was not found.'
            )
        # The `validate` method should return a dictionary of validated data.
        # This is the data that is passed to the `create` and `update` methods
        # that we will see later on.
        return {
            'user': user,
        }
